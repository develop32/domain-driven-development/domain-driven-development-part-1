﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class WeatherLatestView
    Inherits System.Windows.Forms.Form

    'フォームがコンポーネントの一覧をクリーンアップするために dispose をオーバーライドします。
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Windows フォーム デザイナーで必要です。
    Private components As System.ComponentModel.IContainer

    'メモ: 以下のプロシージャは Windows フォーム デザイナーで必要です。
    'Windows フォーム デザイナーを使用して変更できます。  
    'コード エディターを使って変更しないでください。
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.DataDateLabel = New System.Windows.Forms.Label()
        Me.ConditionLabel = New System.Windows.Forms.Label()
        Me.AreaIdTextBox = New System.Windows.Forms.TextBox()
        Me.LatestButton = New System.Windows.Forms.Button()
        Me.TemperatureLabel = New System.Windows.Forms.Label()
        Me.SuspendLayout()
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(13, 54)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(31, 15)
        Me.Label1.TabIndex = 0
        Me.Label1.Text = "地域"
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Location = New System.Drawing.Point(13, 98)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(31, 15)
        Me.Label2.TabIndex = 1
        Me.Label2.Text = "日時"
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Location = New System.Drawing.Point(13, 142)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(31, 15)
        Me.Label3.TabIndex = 2
        Me.Label3.Text = "状態"
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Location = New System.Drawing.Point(13, 186)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(31, 15)
        Me.Label4.TabIndex = 3
        Me.Label4.Text = "温度"
        '
        'DataDateLabel
        '
        Me.DataDateLabel.AutoSize = True
        Me.DataDateLabel.Location = New System.Drawing.Point(64, 98)
        Me.DataDateLabel.Name = "DataDateLabel"
        Me.DataDateLabel.Size = New System.Drawing.Size(45, 15)
        Me.DataDateLabel.TabIndex = 4
        Me.DataDateLabel.Text = "Label5"
        '
        'ConditionLabel
        '
        Me.ConditionLabel.AutoSize = True
        Me.ConditionLabel.Location = New System.Drawing.Point(64, 142)
        Me.ConditionLabel.Name = "ConditionLabel"
        Me.ConditionLabel.Size = New System.Drawing.Size(45, 15)
        Me.ConditionLabel.TabIndex = 5
        Me.ConditionLabel.Text = "Label6"
        '
        'AreaIdTextBox
        '
        Me.AreaIdTextBox.Location = New System.Drawing.Point(64, 51)
        Me.AreaIdTextBox.Margin = New System.Windows.Forms.Padding(3, 4, 3, 4)
        Me.AreaIdTextBox.Name = "AreaIdTextBox"
        Me.AreaIdTextBox.Size = New System.Drawing.Size(116, 23)
        Me.AreaIdTextBox.TabIndex = 6
        '
        'LatestButton
        '
        Me.LatestButton.Location = New System.Drawing.Point(188, 48)
        Me.LatestButton.Margin = New System.Windows.Forms.Padding(3, 4, 3, 4)
        Me.LatestButton.Name = "LatestButton"
        Me.LatestButton.Size = New System.Drawing.Size(87, 29)
        Me.LatestButton.TabIndex = 7
        Me.LatestButton.Text = "直近値"
        Me.LatestButton.UseVisualStyleBackColor = True
        '
        'TemperatureLabel
        '
        Me.TemperatureLabel.AutoSize = True
        Me.TemperatureLabel.Location = New System.Drawing.Point(64, 186)
        Me.TemperatureLabel.Name = "TemperatureLabel"
        Me.TemperatureLabel.Size = New System.Drawing.Size(45, 15)
        Me.TemperatureLabel.TabIndex = 8
        Me.TemperatureLabel.Text = "Label7"
        '
        'WeatherLatestView
        '
        Me.AcceptButton = Me.LatestButton
        Me.AutoScaleDimensions = New System.Drawing.SizeF(7.0!, 15.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(284, 261)
        Me.Controls.Add(Me.TemperatureLabel)
        Me.Controls.Add(Me.LatestButton)
        Me.Controls.Add(Me.AreaIdTextBox)
        Me.Controls.Add(Me.ConditionLabel)
        Me.Controls.Add(Me.DataDateLabel)
        Me.Controls.Add(Me.Label4)
        Me.Controls.Add(Me.Label3)
        Me.Controls.Add(Me.Label2)
        Me.Controls.Add(Me.Label1)
        Me.Font = New System.Drawing.Font("Meiryo UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(128, Byte))
        Me.Margin = New System.Windows.Forms.Padding(3, 4, 3, 4)
        Me.Name = "WeatherLatestView"
        Me.Text = "WeatherLatestView"
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub

    Friend WithEvents Label1 As Label
    Friend WithEvents Label2 As Label
    Friend WithEvents Label3 As Label
    Friend WithEvents Label4 As Label
    Friend WithEvents DataDateLabel As Label
    Friend WithEvents ConditionLabel As Label
    Friend WithEvents AreaIdTextBox As TextBox
    Friend WithEvents LatestButton As Button
    Friend WithEvents TemperatureLabel As Label
End Class
