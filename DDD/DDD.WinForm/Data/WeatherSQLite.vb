﻿Imports System.Data.SQLite

Public Class WeatherSQLite

    Public Shared Function GetLatest(areaId As Integer) As DataTable
        Dim sql As String = "
select DataDate,
       Condition,
       Temperature
from   Weather
where  AreaId = @AreaId
order by DataDate desc
LIMIT 1"
        Dim dt As New DataTable
        Using connection As New SQLiteConnection(CommonConst.ConnectionString)
            Using command As New SQLiteCommand(sql, connection)
                connection.Open()
                command.Parameters.AddWithValue("@AreaId", areaId)
                Using adapter As New SQLiteDataAdapter(command)
                    adapter.Fill(dt)
                End Using
            End Using
        End Using
        Return dt
    End Function

End Class