﻿Public Class CommonFunc

    Public Shared Function RoundString(value As Decimal, decimalPoint As Integer) As String
        Dim temp As Decimal = Convert.ToSingle(Math.Round(value, decimalPoint))
        Return temp.ToString("F" & decimalPoint)
    End Function

End Class