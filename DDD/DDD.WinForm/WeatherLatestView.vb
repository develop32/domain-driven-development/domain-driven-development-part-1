﻿Public Class WeatherLatestView

    Private Sub LatestButton_Click(sender As Object, e As EventArgs) Handles LatestButton.Click

        Dim dt As DataTable = WeatherSQLite.GetLatest(AreaIdTextBox.Text)

        If dt.Rows.Count > 0 Then
            DataDateLabel.Text = dt.Rows(0)("DataDate").ToString
            ConditionLabel.Text = dt.Rows(0)("Condition").ToString
            TemperatureLabel.Text = CommonFunc.RoundString(
                Convert.ToSingle(dt.Rows(0)("Temperature")),
                CommonConst.TemperatureDecimalPoint) &
                CommonConst.TemperatureUnitName
        End If

    End Sub

End Class